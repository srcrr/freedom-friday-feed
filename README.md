# freedom_friday_feed

<p align="center">
    <em>Find promising open source projects for Freedom Friday</em>
</p>

## The Basic Idea

Programmatically query GitHub to find promising up&comming open source projects.

## Requirements

- Python 3.8+
- Poetry
- MongoDB

## Installing fffeed

Clone this repository and install it locally.

```bash

# ensure you have Poetry installed
pip install --user poetry

# install all dependencies (including dev)
poetry install

# develop!

```

## Example Usage

```bash
# I like to use poetry
# This might hang for a bit
poetry run python -m fffeed.fetch 2021-02-01 2019-05-15 -s 1 -S 500 -T 3
poetry run python -m fffeed.report > report.csv
# Or simply print out to the console
poetry run python -m fffeed.report
```

Only **Python 3.6+** is supported as required by the black, pydantic packages

## Contributing Guide

Welcome! 😊👋

> Please see the [Contributing Guide](CONTRIBUTING.md).
