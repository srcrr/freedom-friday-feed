# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.0.1a] - 2021-05-12

- Conception!

[Unreleased]: https://https://src-r-r.github.io/src-r-r/freedom_friday_feed/compare/v0.0.1a...HEAD
[0.0.1a]: https://https://src-r-r.github.io/src-r-r/freedom_friday_feed/releases/tag/v0.0.1a
