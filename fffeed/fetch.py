from pathlib import Path
import asyncio
import typing as T
import click
from .common import PATH_LATEST_REPO_QUERY, URL_GITHUB_GRAPHQL, get_mongo_collection
from .defaults import (
    DEFAULT_MIN_STARS,
    DEFAULT_MAX_STARS,
    DEFAULT_N_TOPICS,
    DEFAULT_FIRST_COUNT,
)
from .env import GITHUB_ACCESS_TOKEN, MONGO_URL, MONGO_COLLECTION_NAME
from .variables import GQL_LATEST_REPO
from .keys import SEARCH_QUERY, FIRST, AFTER
from .util.graphql_session import GraphqlSession
from .models import Repository
import requests
from fffeed.util.validation import validate_date

from mongoengine import connect

import logging

from pymongo import MongoClient, collection

log = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)


def key_change(d: T.Dict, old_key: T.AnyStr, new_key: T.AnyStr):
    d[new_key] = d[old_key]
    del d[old_key]
    return d


async def fetch_latest_repos(
    pushed_date: T.AnyStr,
    created_date: T.AnyStr,
    n_topics=DEFAULT_N_TOPICS,
    min_star=DEFAULT_MIN_STARS,
    max_star=DEFAULT_MAX_STARS,
    first=DEFAULT_FIRST_COUNT,
    after=None,
):

    # Format the search query
    search_query = GQL_LATEST_REPO.format(
        pushed_date=pushed_date,
        min_stars=min_star,
        max_stars=max_star,
        created_date=created_date,
        n_topics=n_topics,
    )

    # Format the graphql query from the search query
    gql_session = GraphqlSession(URL_GITHUB_GRAPHQL, GITHUB_ACCESS_TOKEN)
    with open(PATH_LATEST_REPO_QUERY, "r") as f:
        query_string = f.read()

    # Construct the variables
    variables = {
        SEARCH_QUERY: search_query,
        FIRST: first,
    }
    if after:
        variables[AFTER] = after
    log.debug(query_string)
    # log.debug(variables)
    return await gql_session.query(query_string, variables)


async def store_data_in_db(data):
    connect(MONGO_URL)
    for edge in data["data"]["search"]["edges"]:
        repo = Repository.from_graphql(edge)
        log.info("Create repository %s", repo)


async def run_engine(
    pushed_date: T.AnyStr,
    created_date: T.AnyStr,
    topics=DEFAULT_N_TOPICS,
    min_stars=DEFAULT_MIN_STARS,
    max_stars=DEFAULT_MAX_STARS,
    first=DEFAULT_FIRST_COUNT,
    after=None,
):
    data = await fetch_latest_repos(
        pushed_date,
        created_date,
        topics,
        min_stars,
        max_stars,
        first,
        after,
    )
    if "errors" in data:
        raise RuntimeError(data["errors"])
    await store_data_in_db(data)


@click.command()
@click.argument("pushed_date", callback=validate_date)
@click.argument("created_date", callback=validate_date)
@click.option("-T", "--topics", default=DEFAULT_N_TOPICS, type=int)
@click.option("-s", "--min-stars", default=DEFAULT_MIN_STARS, type=int)
@click.option("-S", "--max-stars", default=DEFAULT_MAX_STARS, type=int)
@click.option("-f", "--first", default=DEFAULT_FIRST_COUNT, type=int)
@click.option("-a", "--after", default=None)
def invoke_run_engine(
    pushed_date: T.AnyStr,
    created_date: T.AnyStr,
    topics=DEFAULT_N_TOPICS,
    min_stars=DEFAULT_MIN_STARS,
    max_stars=DEFAULT_MAX_STARS,
    first=DEFAULT_FIRST_COUNT,
    after=None,
):
    asyncio.run(
        run_engine(
            pushed_date, created_date, topics, min_stars, max_stars, first, after
        )
    )


if __name__ == "__main__":
    invoke_run_engine()  # pylint:disable=no-value-for-parameter
