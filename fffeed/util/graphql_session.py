import typing as T
import requests
import logging

log = logging.getLogger(__name__)


class GraphqlSession:
    def __init__(self, endpoint: T.AnyStr, bearer_token: T.AnyStr = None) -> T.Dict:
        self.endpoint = endpoint
        self.bearer_token = bearer_token

    @property
    def headers(self):
        return {"Authorization": f"bearer {self.bearer_token}"}

    async def query(self, query: T.AnyStr, variables: T.Dict = None):
        data = {
            "query": query,
            "variables": variables,
        }
        res = requests.post(self.endpoint, json=data, headers=self.headers)
        if res.status_code >= 300:
            raise RuntimeError(res.content)
        # log.debug(res.content)
        return res.json()
