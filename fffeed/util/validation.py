from datetime import datetime
import typing as T
from fffeed.common import VALID_DATE_FORMAT

def validate_date(ctx, param, value : T.AnyStr) -> T.AnyStr:
    datetime.strptime(value, VALID_DATE_FORMAT)
    return value