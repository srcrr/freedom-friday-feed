import math

import logging

log = logging.getLogger(__name__)


def closure(closed_issues, total_issues):
    return (closed_issues + 1) / (total_issues + 1)


def activity(watches, forks, mentionable_users):
    return watches * forks * mentionable_users


def score(**kwargs):
    issues_total = kwargs["total_issues"]
    closed_issues = kwargs["closed_issues"]
    sg_count = kwargs["stargazer_count"]
    releases = kwargs["releases"]
    if not issues_total:
        return 0
    closure = (closed_issues / issues_total) or 1.0
    res = math.log((sg_count / closure * releases) ** releases)
    log.debug("(%f / %f * %f) ** %f = %f", sg_count, closure, releases, releases, res)
    return res
