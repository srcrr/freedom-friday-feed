from pathlib import Path
import asyncio
import typing as T
from .common import PATH_LATEST_REPO_QUERY, URL_GITHUB_GRAPHQL, get_mongo_collection
from .env import GITHUB_ACCESS_TOKEN, MONGO_URL, MONGO_COLLECTION_NAME
from .variables import GQL_LATEST_REPO
from .keys import SEARCH_QUERY
from .util.graphql_session import GraphqlSession
from .util.math import score
import requests
from mongoengine import *
from .models import Repository, RepositoryStats

import logging

import csv
import sys
import math

from pymongo import MongoClient, collection

log = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)

DELIMITER="\t"

HEADERS = (
    "name",
    "total_issues",
    "closed_issues",
    "stargazers",
    "watchers",
    "forks",
    "mentionable_users",
    "releases",
    "activity",
    "closure",
    "score",
    "last_updated",
    "url",
    "description",
    "topics",
    "_id",
)


async def fetch_from_mongo(outfile=sys.stdout):
    curs = connect(MONGO_URL)
    writer = csv.DictWriter(outfile, HEADERS, delimiter=DELIMITER)
    writer.writeheader()
    for repo in Repository.objects:
        row = dict([(k, getattr(repo, k)) for k in HEADERS if hasattr(repo, k)])
        # also copy over the stats
        row.update(
            dict(
                [(k, getattr(repo.stats, k)) for k in HEADERS if hasattr(repo.stats, k)]
            )
        )
        row["topics"] = ','.join(row["topics"])
        writer.writerow(row)
    curs.close()


async def main():
    await fetch_from_mongo()


if __name__ == "__main__":
    asyncio.run(main())
