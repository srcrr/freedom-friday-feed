import logging
import typing as T
from datetime import datetime
from fffeed.common import TIMESTAMP_FORMAT
from mongoengine import *
import math

log = logging.getLogger(__name__)

TDate = T.Any

# How the weights affect scoring
CLOSURE_FACTOR = 65
TOTAL_ISSUES_FACTOR = 90
RELEASES_FACTOR = 90
MENTIONABLE_USERS_FACTOR = 50
WATCHERS_FACTOR = 45
FORKS_FACTOR = 30
STARGAZERS_FACTOR = 25
AGE_FACTOR = 3
CLOSED_ISSUES_FACTOR = 1


class FFMixin(object):
    pass


class RepositoryStats(EmbeddedDocument, FFMixin):

    watchers = IntField(required=True)
    stargazers = IntField(required=True)
    forks = IntField(required=True)
    mentionable_users = IntField(required=True)
    total_issues = IntField(required=True)
    closed_issues = IntField(required=True)
    releases = IntField(required=True)
    last_updated = DateTimeField(required=True)

    @property
    def closure(self) -> float:
        return float(self.closed_issues + 1) / float(self.total_issues + 1)

    @property
    def activity(self) -> float:
        return float(
            (self.mentionable_users * 1)
            * (self.forks * 1)
            * (self.releases * 1)
            * (self.watchers * 1)
            * (self.stargazers * 1)
        )

    @property
    def age_in_days(self) -> float:
        return (datetime.today() - self.last_updated).days + 1

    @property
    def score(self) -> float:
        factor = self.activity / (self.closure)
        if self.age_in_days > 0:
            factor = (factor / self.age_in_days / AGE_FACTOR)
        if not (self.releases and factor):
            return self.releases or factor
        try:
            return math.log(factor * self.releases)
        except ValueError as err:
            import ipdb

            ipdb.set_trace()


class Repository(Document, FFMixin):

    _id = StringField()
    name = StringField(required=True)
    url = URLField(required=True)
    description = StringField()
    stats = EmbeddedDocumentField(RepositoryStats)
    topics = ListField()

    def __str__(self):
        return f'<Repository id="{self._id}", name="{self.name}">'

    def __unicode__(self):
        return str(self)

    def __repr__(self):
        return str(self)

    @classmethod
    def from_graphql(cls, edge: T.Dict, autosave=True):
        node = edge["node"]
        id = node["id"]
        name = node["name"]
        url = node["url"]
        last_updated = datetime.strptime(node["updatedAt"], TIMESTAMP_FORMAT)
        description = node["description"]
        watchers = node["watchers"]["totalCount"]
        forks = node["forkCount"]
        mentionable_users = node["mentionableUsers"]["totalCount"]
        closed_issues = node["closedIssues"]["totalCount"]
        total_issues = node["totalIssues"]["totalCount"]
        topics = [t["node"]["topic"]["name"] for t in node["repositoryTopics"]["edges"]]
        releases = node["releases"]["totalCount"]
        stargazers = node["stargazerCount"]

        stats = RepositoryStats(
            watchers=watchers,
            forks=forks,
            mentionable_users=mentionable_users,
            closed_issues=closed_issues,
            total_issues=total_issues,
            releases=releases,
            last_updated=last_updated,
            stargazers=stargazers,
        )
        repo = cls(
            _id=id,
            name=name,
            url=url,
            description=description,
            stats=stats,
            topics=topics,
        )

        if autosave:
            repo.save()
        return repo