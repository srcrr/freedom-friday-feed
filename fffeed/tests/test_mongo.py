from fffeed.common import get_mongo_collection
import unittest

import logging
log = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)

class TestMongo(unittest.TestCase):
    def test_get_collection(self):
        col = get_mongo_collection()
        self.assertIsNotNone(col)