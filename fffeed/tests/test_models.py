from unittest import TestCase
from mongoengine import connect
from pymongo import MongoClient
from fffeed.models import Repository, RepositoryStats
from pathlib import Path
import json

import logging

log = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)

# Common paths that will be used
# TODO: maybe refactor?
HERE = Path(__file__).parent.resolve()
MOCK_DIR = HERE / "mock"
JSON_RESPONSE_1 = MOCK_DIR / "response-1.json"

class TestModels(TestCase):

    DB_NAME = "test_fffeed"

    def setUp(self):
        self.curs = connect(self.DB_NAME)

    def test_create_from_graphql(self):
        with open(JSON_RESPONSE_1) as f:
            mock_data_1 = json.load(f)
        edges = mock_data_1["data"]["search"]["edges"]
        for edge in edges:
            rep = Repository.from_graphql(edge)
        self.assertTrue(True)
    
    def tearDown(self):
        self.curs.drop_database(self.DB_NAME)
        self.curs.close()
    
