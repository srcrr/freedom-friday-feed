from fffeed.util.math import score
import unittest

import logging
log = logging.getLogger(__name__)

logging.basicConfig(level=logging.DEBUG)

class TestUtils(unittest.TestCase):
    def _mock_score_input(self, total_issues, closed_issues, stargazer_count, releases):
        return {
            "total_issues": total_issues,
            "closed_issues": closed_issues,
            "stargazerCount": stargazer_count,
            "releases": releases,
        }

    def test_score(self):
        a = self._mock_score_input(0, 0, 10, 0)
        b = self._mock_score_input(10, 1, 10, 0)
        c = self._mock_score_input(10, 9, 10, 0)
        d = self._mock_score_input(10, 10, 10, 1)
        e = self._mock_score_input(10, 10, 13, 9)
        f = self._mock_score_input(10, 10, 10, 10)
        self.assertLessEqual(score(**a), score(**b))
        self.assertLessEqual(score(**b), score(**c))
        self.assertLessEqual(score(**c), score(**d))
        self.assertLessEqual(score(**d), score(**e))
        self.assertLessEqual(score(**e), score(**f))