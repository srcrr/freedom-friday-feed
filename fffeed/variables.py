# Story any Graphql search variables here

_EXAMPLE_PUSHED_DATE = "2021-04-01"
_EXAMPLE_STAR_RANGE = [25, 50]
_EXAMPLE_CREATED_DATE = "2019-04-01"
_EXAMPLE_N_TOPICS = 5

GQL_LATEST_REPO = 'is:public pushed:>={pushed_date} stars:{min_stars}..{max_stars} sort:interactions-desc topics:>={n_topics} created:>={created_date} NOT module NOT plugin NOT wrapper NOT "for the"'
