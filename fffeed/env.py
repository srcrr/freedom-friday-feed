
from dotenv import load_dotenv
import os

from .defaults import DEFAULT_MONGO_COLLECTION_NAME

load_dotenv()

GITHUB_ACCESS_TOKEN = os.getenv("GITHUB_ACCESS_TOKEN")
MONGO_URL = os.getenv("MONGO_URL")
MONGO_COLLECTION_NAME = os.getenv("MONGO_COLLECTON_NAME") or DEFAULT_MONGO_COLLECTION_NAME