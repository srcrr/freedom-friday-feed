from pathlib import Path
import os
from pymongo import MongoClient, collection
from .env import MONGO_URL, MONGO_COLLECTION_NAME

VALID_DATE_FORMAT = r"%Y-%m-%d"
TIMESTAMP_FORMAT = r"%Y-%m-%dT%H:%M:%SZ"

# Common directories
HERE = Path(os.path.dirname(__file__)).absolute()
QUERIES_DIR = HERE / "queries"
GQL_GH_QUERIES_DIR = QUERIES_DIR / "github"
PATH_LATEST_REPO_QUERY = GQL_GH_QUERIES_DIR / "latest-repositories.graphql"

# Common URLs
URL_GITHUB_GRAPHQL = "https://api.github.com/graphql"


def get_mongo_collection() -> collection.Collection:
    client = MongoClient(MONGO_URL)
    col = client.db[MONGO_COLLECTION_NAME]
    return col